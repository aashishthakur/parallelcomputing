<h2>Largest Triangle problem and its solution</h2>
<p><a name="system"></a></p>
<h3>Problem: </h3>
<p>Given a group of two-dimensional points, we want to find the&nbsp;<strong>largest triangle,</strong>&nbsp;namely three distinct points that are the vertices of a triangle with the largest area. The area of a triangle is (<em>s</em>(<em>s</em>&nbsp;&minus;&nbsp;<em>a</em>)(<em>s</em>&nbsp;&minus;&nbsp;<em>b</em>)(<em>s</em>&nbsp;&minus;&nbsp;<em>c</em>))<sup>1/2</sup>, where&nbsp;<em>a</em>,&nbsp;<em>b</em>, and&nbsp;<em>c</em>&nbsp;are the lengths of the triangle's sides and&nbsp;<em>s</em>&nbsp;=&nbsp;(<em>a</em>&nbsp;+&nbsp;<em>b</em>&nbsp;+&nbsp;<em>c</em>)/2. The length of a triangle's side is the Euclidean distance between the side's two endpoints.</p>
<p>You will write sequential and cluster parallel versions of a program that finds the largest triangle in a group of points. The program's command line argument is a&nbsp;<em>constructor expression</em>&nbsp;for a PointSpec object from which the program obtains the points' (<em>x,y</em>) coordinates. </p>

<h3>Solution: </h3>
<p>Approach: </p>
<p>Here, it was important to make sure all the points are covered and have a parallel design.Also it was also important to make sure we do not go over duplicates, areas where two sides are equal or a combinaiton of points which will never form a triangle. Thus, respective checks and loop start and end points were introduced.</p>
<p>In order to process it somewhat equally on all the nodes, the given number of points were divided into chunks. But it was a possibility that the number of points won't be divisible by the number of nodes. Thus, in order to incorporate this, an array filled with all the starting points for nodes is made and scattered to the nodes using scatter function in MPI. Now, to figure out what will be the end points of these nodes, we check what is the starting value, depending on this we understand if we have reached the last node or not.(Last index is stored in a variable for comparison)</p>
<p>These nodes now process the points in the given range and return what is the maximum value of area for them, with the help of reduce, the maximum area out of all the maximum areas of each node is found and shared with all the nodes. This is then used to find out which nodes have maximum area, and figure out the minimum points.</p>

<p>Output for 3000 points, seed is 142857</p>
<p>1577 0.0038473 79.791
2489 99.531 99.740
2924 96.984 0.14588
4930.8
8473 ms</p>