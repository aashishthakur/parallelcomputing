package edu.rit.cs;


import mpi.*;

import java.util.TreeMap;

/**
 * This class finds the largest triangle, given a group of two-dimensional points
 * namely three distinct points that are the vertices of a triangle with the largest
 * area. The area of a triangle is given in the assignment description, where a, b, and c
 * are the lengths of the triangle's sides and s = (a + b + c)/2.
 *
 * @author Aashish Thakur, at1948@rit.edu
 * @since 11-October-2019
 */
public class LargeTriangleParallel {

    /**
     * The main class, which calls a method in order to find out the points
     * which return the maximum area after taking in input of the number
     * of points, sides, and seed from the user.
     *
     * @param args Used to take in the number of points, side,
     *             and seed.
     */
    public static void main(String[] args) throws MPIException {

        long t1 = System.currentTimeMillis();
        int numPoints = 0, side = 0, seed = 0;
        try {
            numPoints = Integer.parseInt(args[0]);
            side = Integer.parseInt(args[1]);
            seed = Integer.parseInt(args[2]);


        } catch (NumberFormatException e) {
            //In case numbers are not entered.
            System.err.println("Please enter number of points, Side, " +
                    "and Seed as integer numbers.");
            System.exit(1);
        }

        findMaxArea(numPoints,args,side,seed,t1);
    }


    /**
     * Checks if the two given sides have equal value or not.
     *
     * @param a The first point for which the test is done.
     * @param b The first point for which the test is done.
     * @return True if there is a match, else false.
     */
    private static boolean sameSides(Point a, Point b) {
        return (a.getX() == b.getX()) && (a.getY() == b.getY());
    }

    /**
     * Finds the maximum area using multiple nodes. Distributes the number of points
     * over the nodes depending on the number of nodes available and then reduces
     * their output in order to get maximum area, and the points which represent
     * that area.
     *
     * @param numPoints     Total number of points
     * @param args          Used to take in the number of points, side,
     *                      and seed.
     * @param seed          Total sides.
     * @param side          Seed value for random.
     * @param t1            Start time.
     *
     */
    private static void findMaxArea (int numPoints , String[] args, int side, int seed, long t1) throws MPIException{
        Point[] randomPoints = new Point[numPoints];

        //Three instances of random points for three different sides.
        RandomPoints rndPoints1 = new RandomPoints(numPoints, side, seed);
        int index = 0;

        //Prefetch the values of the points in an array.
        while (rndPoints1.hasNext()) {
            randomPoints[index] = rndPoints1.next();
            index++;
        }
        //Global array to store the reduced max area.
        double[] global_max = new double[1];
        int[] global_ind = new int[1];
        //Initialize the MPI with user arguments.
        MPI.Init(args);
        //Help with chunk size
        int size = MPI.COMM_WORLD.getSize();
        //Get the chunk size
        int chunk = numPoints / size;
        int[] sendCount = new int[size];
        int[] receiveCount = new int[1];
        int []idx1 = {0};
        int []idx2 = {0};
        int []idx3 = {0};
        int []temp = {0};
        //Three points to represent a triangle.
        Point a, b, c;
        double[] maxArea = {0.0};
        double area, sideAB, sideBC, sideAC;

        //Treemap to store the points in a sorted way.
        TreeMap<Integer, Point> h = new TreeMap<>();

        //Calculate the starting points of various chunks
        //for eg. 0, 25, 50, etc.
        int j = 0, lastVal = 0;
        for (int i = 0; i < size; i++) {
            if (i == size - 1) {
                lastVal = j;
            }
            sendCount[i] = j;

            j = j + chunk;
        }

        //Appropriate chunks to send for parallel processing using scatter.
        MPI.COMM_WORLD.scatter(sendCount, 1, MPI.INT, receiveCount, 1, MPI.INT, 0);


        // Loop over all the different pair of points in order to find and cover
        // all the possibilities.
        //Figure out what is the end range for it.
        int maxInd = receiveCount[0] != lastVal ? receiveCount[0] + chunk : numPoints - 1;
        for (index = receiveCount[0]; index < maxInd; index++) {
            a = randomPoints[index];
            for (j = index+1; j < randomPoints.length; j++) {
                b = randomPoints[j];
                // In case both points are same, it means we will
                // have a narrow triangle and thus skip it.
                if (sameSides(a, b)) {
                    continue;
                }
                for (int k = j+1; k < randomPoints.length; k++) {
                    c = randomPoints[k];
                    if (sameSides(a, c) || sameSides(b, c)) {
                        continue;
                    }
                    //Use euclidean distance to find the values of the sides.
                    sideAB = euclideanDistance(a, b);
                    sideBC = euclideanDistance(b, c);
                    sideAC = euclideanDistance(a, c);
                    if ((!(sideAB+sideAC>sideBC)||
                            !(sideAB+sideBC>sideAC)||!(sideBC+sideAC>sideAB))){
//                        System.out.println("yes");
                        continue;
                    }
                    area = calcArea(sideAB, sideBC, sideAC);
                    //Find the maximum area and the indexes for them.
                    if (area > maxArea[0]) {

                        maxArea[0] = area;

                        idx1[0] = index+1;
                        idx2[0] = j+1;
                        idx3[0] = k+1;
                        h.put(index+1, a);
                        h.put(j+1, b);
                        h.put(k+1, c);

                    }
                }
            }

        }

        // Reduce the max areas obtained from all nodes in order to get the maximum
        // area, then send it to all other nodes using reduce all.
        MPI.COMM_WORLD.allReduce(maxArea, global_max, 1, MPI.DOUBLE, MPI.MAX);

        // In case the current node matches with max area, find out if these
        // are the best points.
        if (maxArea[0]==global_max[0]) {
            temp[0] = idx1[0];
        }

        // Finds the minimum value for the first point.
        MPI.COMM_WORLD.allReduce(temp, global_ind, 1, MPI.DOUBLE, MPI.MAX);

        // The nodes which have this value will now check for the second point
        // This will then be repeated for the third point as well.
        if (maxArea[0]==global_max[0] && idx1[0]==global_ind[0]){
            temp[0]=idx2[0];
        }
        MPI.COMM_WORLD.allReduce(temp, global_ind, 1, MPI.DOUBLE, MPI.MAX);
        if (maxArea[0]==global_max[0] && idx2[0]==global_ind[0]){
            temp[0]=idx3[0];
        }
        MPI.COMM_WORLD.allReduce(temp, global_ind, 1, MPI.DOUBLE, MPI.MAX);
        //When all the points are found, print them and stop the clock.
        if (maxArea[0]==global_max[0] && idx3[0]==global_ind[0]) {
            System.out.printf ("%d %.5g %.5g%n", idx1[0], h.get(idx1[0]).getX(), h.get(idx1[0]).getY());
            System.out.printf ("%d %.5g %.5g%n", idx2[0], h.get(idx2[0]).getX(), h.get(idx2[0]).getY());
            System.out.printf ("%d %.5g %.5g%n", idx3[0], h.get(idx3[0]).getX(), h.get(idx3[0]).getY());
            System.out.printf ("%.5g%n", global_max[0]);
            //System.out.println(System.currentTimeMillis()-t1);

        }

        MPI.COMM_WORLD.barrier();
        MPI.Finalize();

    }


    /**
     * Calculates the area of the triangle using, where a, b, and c are the lengths
     * of the triangle's sides and s = (a + b + c)/2.
     *
     * @param a lengths of the triangle side AB
     * @param b lengths of the triangle side BC
     * @param c lengths of the triangle side AC
     * @return Returns the area of the triangle.
     */
    public static double calcArea(double a, double b, double c) {
        double s = (a + b + c) / 2;
        return Math.sqrt(s * (s - a) * (s - b) * (s - c));
    }


    /**
     * Returns the Euclidean distance between two given points
     *
     * @param one The first point containing X and Y coordinates.
     * @param two The second point containing X and Y coordinates.
     * @return The distance is returned.
     */
    public static double euclideanDistance(Point one, Point two) {

        return Math.sqrt(Math.pow((one.getX() - two.getX()), 2) +
                Math.pow((one.getY() - two.getY()), 2));
    }
}
