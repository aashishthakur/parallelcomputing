package edu.rit.cs;


import java.text.DecimalFormat;
import java.util.TreeMap;

/**
 *
 * This class finds the largest triangle, given a group of two-dimensional points
 * namely three distinct points that are the vertices of a triangle with the largest
 * area. The area of a triangle is (s(s − a)(s − b)(s − c))1/2, where a, b, and c
 * are the lengths of the triangle's sides and s = (a + b + c)/2.
 *
 * @author Aashish Thakur, at1948@rit.edu
 * @since 08-October-2019
 *
 *
 */
public class LargeTriangle {

    /**
     * The main class, which calls a method in order to find out the points
     * which return the maximum area after taking in input of the number
     * of points, sides, and seed from the user.
     *
     * @param args              Used to take in the number of points, side,
     *                          and seed.
     */
    public static void main(String[] args) {

        int numPoints = 0, side = 0, seed = 0;
        try {
            numPoints = Integer.parseInt(args[0]);
            side = Integer.parseInt(args[1]);
            seed = Integer.parseInt(args[2]);

        } catch (NumberFormatException e) {
            //In case numbers are not entered.
            System.err.println("Please enter number of points, Side, " +
                    "and Seed as integer numbers.");
            System.exit(1);
        }
        //Finds the largest triangle in terms of area.
        findMaxArea(numPoints, side, seed);
    }


    /**
     * Checks if the two given sides have equal value or not.
     *
     * @param a         The first point for which the test is done.
     * @param b         The first point for which the test is done.
     * @return          True if there is a match, else false.
     *
     */
    private static boolean sameSides(Point a, Point b) {
        return (a.getX() == b.getX()) && (a.getY() == b.getY());
    }

    /**
     * Finds the maximum area.
     *
     * @param numPoints              Total number of points
     * @param side                   Total sides
     * @param seed                   Seed value for random
     *
     */
    private static void findMaxArea(int numPoints,int side,int seed){
        //Indexes in order to keep track of where the points are.
        int idx1 = 1, idx2, idx3;

        //For formatting
        DecimalFormat df = new DecimalFormat("#.#");

        Point a, b, c;
        double maxArea = 0.0;
        double area, sideAB, sideBC, sideAC;
        //Treemap to store the points in a sorted way.
        TreeMap<Integer, Point> h = new TreeMap<>();


        //Three instances of randompoints for three different sides.
        RandomPoints rndPoints1 = new RandomPoints(numPoints, side, seed);
        RandomPoints rndPoints2 = new RandomPoints(numPoints, side, seed);
        RandomPoints rndPoints3 = new RandomPoints(numPoints, side, seed);

        // Loop over all the different points in order to find and cover
        // all the possibilities.
        while (rndPoints1.hasNext()) {
            a = rndPoints1.next();
            idx2 = 1;
            while (rndPoints2.hasNext()) {
                b = rndPoints2.next();
                // In case both points are same, it means we will
                // have a narrow triangle and thus skip it.
                if (sameSides(a, b)) {
                    idx2++;
                    continue;
                }
                idx3 = 1;
                while (rndPoints3.hasNext()) {
                    c = rndPoints3.next();
                    if (sameSides(a, c) || sameSides(b, c)) {
                        idx3++;
                        continue;
                    }
                    //Use euclidean distance to find the values of the sides.
                    sideAB = euclideanDistance(a, b);
                    sideBC = euclideanDistance(b, c);
                    sideAC = euclideanDistance(a, c);
                    area = calcArea(sideAB, sideBC, sideAC);
                    //Find the maximum area and the indexes for them.
                    if (area > maxArea) {
                        maxArea = area;
                        h.clear();
                        h.put(idx1, a);
                        h.put(idx2, b);
                        h.put(idx3, c);

                    }
                    idx3++;
                }
                rndPoints3 = new RandomPoints(numPoints, side, seed);
                idx2++;
            }

            rndPoints2 = new RandomPoints(numPoints, side, seed);
            idx1++;

        }

        //Print out all the points that represent the max area.
        h.forEach((k, v) -> {
            System.out.printf ("%d %.5g %.5g%n", k, v.getX(), v.getY());
        });

        System.out.println(df.format(maxArea));

    }


    /**
     * Calculates the area of the triangle using s (s(s − a)(s − b)(s − c))1/2, where a, b, and c are the lengths
     * of the triangle's sides and s = (a + b + c)/2.
     *
     *
     * @param a     lengths of the triangle side AB
     * @param b     lengths of the triangle side BC
     * @param c     lengths of the triangle side AC
     * @return      Returns the area of the triangle.
     */
    private static double calcArea(double a, double b, double c) {
        double s = (a + b + c) / 2;
        return Math.sqrt(s * (s - a) * (s - b) * (s - c));
    }


    /**
     * Returns the Euclidean distance between two given points
     *
     * @param one       The first point containing X and Y coordinates.
     * @param two       The second point containing X and Y coordinates.
     * @return          The distance is returned.
     */
    private static double euclideanDistance(Point one, Point two) {

        return Math.sqrt(Math.pow((one.getX() - two.getX()), 2) +
                Math.pow((one.getY() - two.getY()), 2));
    }
}
