package edu.rit.cs;

//
//
import mpi.*;
//
//import java.util.TreeMap;
//
/**
 * This class finds the largest triangle, given a group of two-dimensional points
 * namely three distinct points that are the vertices of a triangle with the largest
 * area. The area of a triangle is given in the assignment description, where a, b, and c
 * are the lengths of the triangle's sides and s = (a + b + c)/2.
 *
 * @author Aashish Thakur, at1948@rit.edu
 * @since 11-October-2019
 */
public class coin {
//
//    /**
//     * The main class, which calls a method in order to find out the points
//     * which return the maximum area after taking in input of the number
//     * of points, sides, and seed from the user.
//     *
//     * @param args Used to take in the number of points, side,
//     *             and seed.
//     */
    public static void main(String[] args) throws MPIException {

        calc(args);
    }
//
//
//    /**
//     * Checks if the two given sides have equal value or not.
//     *
//     * @param a The first point for which the test is done.
//     * @param b The first point for which the test is done.
//     * @return True if there is a match, else false.
//     */
//    private static boolean sameSides(Point a, Point b) {
//        return (a.getX() == b.getX()) && (a.getY() == b.getY());
//    }
//
//    /**
//     * Finds the maximum area using multiple nodes. Distributes the number of points
//     * over the nodes depending on the number of nodes available and then reduces
//     * their output in order to get maximum area, and the points which represent
//     * that area.
//     *
//     * @param numPoints     Total number of points
//     * @param args          Used to take in the number of points, side,
//     *                      and seed.
//     * @param seed          Total sides.
//     * @param side          Seed value for random.
//     * @param t1            Start time.
//     *
//     */
    private static void calc (String[] args) throws MPIException{
        System.out.println("Performing Proof-of-Work...wait...");
        int nonce=0;
        String tmp_hash="undefined";

        MPI.Init(args);
        //num of nodes
        int size = MPI.COMM_WORLD.getSize();
        int rank = MPI.COMM_WORLD.getRank();
        Comm comm = MPI.COMM_WORLD;
        double[] val = new double [2];
        if (rank == 0) {
            double chunk = ((double) (Integer.MAX_VALUE / size) - (double) (Integer.MIN_VALUE / size)) + 1;
            double[] temp = new double [2];
            System.out.println(size);

            System.out.println(temp[0]);
            temp[0] = Integer.MIN_VALUE;
            temp[1] = Integer.MIN_VALUE+chunk;
            for (int i = 1; i < size; i++) {
                comm.send(temp, 2,MPI.DOUBLE , i, 0);
                temp[0] = temp[1]+1;
                temp[1] = temp[0]+chunk;
            }

        }
        else{
            comm.recv(val, 2, MPI.DOUBLE, 0, 0);
            System.out.println("\nstart is :"+val[0]);
            System.out.println("end is :"+val[1]);

        }

        MPI.COMM_WORLD.barrier();
        MPI.Finalize();
    }
}
