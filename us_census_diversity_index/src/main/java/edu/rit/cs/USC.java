package edu.rit.cs;

//Id,ProductId,UserId,ProfileName,HelpfulnessNumerator,HelpfulnessDenominator,Score,Time,Summary,Text
public class USC {


    private String STNAME;
    private String CTYNAME;
    private int AGEGRP		;
    private int YEAR		;
    private int	WA_MALE		;
    private int	WA_FEMALE	;
    private int	BA_MALE		;
    private int	BA_FEMALE	;
    private int	IA_MALE		;
    private int	IA_FEMALE	;
    private int	AA_MALE		;
    private int	AA_FEMALE	;
    private int	NA_MALE		;
    private int	NA_FEMALE	;
    private int	TOM_MALE	;
    private int	TOM_FEMALE ;

    public USC(String reviewLine) {
//        String otherThanQuote = " [^\"] ";
//        String quotedString = String.format(" \" %s* \" ", otherThanQuote);
        String regex = ",";

        String [] data = reviewLine.split(regex, -1);

        try {

            this.STNAME = data[3];
            this.CTYNAME = data[4];
            this.YEAR = Integer.valueOf(data[5]);
            this.AGEGRP = Integer.valueOf(data[6]);
            this.WA_MALE = Integer.valueOf(data[10]);
            this.WA_FEMALE = Integer.valueOf(data[11]);
            this.BA_MALE = Integer.valueOf(data[12]);
            this.BA_FEMALE = Integer.valueOf(data[13]);
            this.IA_MALE = Integer.valueOf(data[14]);
            this.IA_FEMALE = Integer.valueOf(data[15]);
            this.AA_MALE = Integer.valueOf(data[16]);
            this.AA_FEMALE = Integer.valueOf(data[17]);
            this.NA_MALE  = Integer.valueOf(data[18]);
            this.NA_FEMALE = Integer.valueOf(data[19]);
            this.TOM_MALE = Integer.valueOf(data[20]);
            this.TOM_FEMALE = Integer.valueOf(data[21]);

        }catch(Exception e){
            System.err.println(e.toString());
        }
    }


    public int getAGEGRP() {
        return AGEGRP;
    }

    public int getAA_FEMALE() {
        return AA_FEMALE;
    }
}
