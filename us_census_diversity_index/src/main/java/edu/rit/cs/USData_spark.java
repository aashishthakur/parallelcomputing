//USData_spark
package edu.rit.cs;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;

import java.io.File;


/**
 * Run the program on a cluster parallel computer to calculate diversity index D for a population, which is
 * the probability that two randomly chosen individuals in that population will be of different races.
 *
 * @author Aashish Thakur
 * @since  12-Nov-2019
 */
public class USData_spark {

    // Uncomment this if you run this example within IntelliJ IDE
    private static final String IDE_Prefix = "word_count/";

    // File address.
    public static final String OutputDirectory = "dataset/review-outputs";
    public static final String DatasetFile = "dataset/amazon-fine-food-reviews/cc-est2017-alldata.csv";

    /**
     * Delete the directory if it exists.     *
     *
     * @param directoryToBeDeleted          File which has to be deleted.
     * @return
     */
    public static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null)
            for (File file : allContents)
                deleteDirectory(file);
        return directoryToBeDeleted.delete();
    }

    /**
     * Diversity index calulated using Spark, by first setting up data and then
     * perform various operations to get the diversity index using the formula,
     *
     *        D  =  1/T^2 Σ Ni (T − Ni)
     *
     * @param spark             Spark session
     */
    public static void USDiversityIndex(SparkSession spark) {


        Dataset ds = spark.read()
                .option("header", "true")
                .option("delimiter", ",")
                .option("inferSchema", "true")
                .csv(IDE_Prefix + DatasetFile);

        //Create a dataset with columns available.
        ds = ds.withColumn("STNAME", ds.col("STNAME").cast(DataTypes.StringType))
                .withColumn("CTYNAME", ds.col("CTYNAME").cast(DataTypes.StringType))
                .withColumn("YEAR", ds.col("YEAR").cast(DataTypes.IntegerType))
                .withColumn("AGEGRP", ds.col("AGEGRP").cast(DataTypes.IntegerType))
                .withColumn("WA_MALE", ds.col("WA_MALE").cast(DataTypes.IntegerType))
                .withColumn("WA_FEMALE", ds.col("WA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("BA_MALE", ds.col("BA_MALE").cast(DataTypes.IntegerType))
                .withColumn("BA_FEMALE", ds.col("BA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("IA_MALE", ds.col("IA_MALE").cast(DataTypes.IntegerType))
                .withColumn("IA_FEMALE", ds.col("IA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("AA_MALE", ds.col("AA_MALE").cast(DataTypes.IntegerType))
                .withColumn("AA_FEMALE", ds.col("AA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("NA_MALE", ds.col("NA_MALE").cast(DataTypes.IntegerType))
                .withColumn("NA_FEMALE", ds.col("NA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("TOM_MALE", ds.col("TOM_MALE").cast(DataTypes.IntegerType))
                .withColumn("TOM_FEMALE", ds.col("TOM_FEMALE").cast(DataTypes.IntegerType));

        // Filter dataset to get values which has age group as 0.
        Dataset ds2 = ds.filter(ds.col("AGEGRP").equalTo(0)).select("STNAME", "CTYNAME",
                "YEAR", "AGEGRP", "WA_MALE", "WA_FEMALE", "BA_MALE", "BA_FEMALE", "IA_MALE", "IA_FEMALE"
                , "AA_MALE", "AA_FEMALE", "NA_MALE", "NA_FEMALE", "TOM_MALE", "TOM_FEMALE")
                .groupBy("STNAME", "CTYNAME")
                .sum();

        // Get a new dataset with the sume of male and female from each race
        Dataset ds3 = ds2.withColumn("WA", ds2.col("sum(WA_MALE)").plus(ds2.col("sum(WA_FEMALE)")))
                .withColumn("BA", ds2.col("sum(BA_MALE)").plus(ds2.col("sum(BA_FEMALE)")))
                .withColumn("IA", ds2.col("sum(IA_MALE)").plus(ds2.col("sum(IA_FEMALE)")))
                .withColumn("AA", ds2.col("sum(AA_MALE)").plus(ds2.col("sum(AA_FEMALE)")))
                .withColumn("NA", ds2.col("sum(NA_MALE)").plus(ds2.col("sum(NA_FEMALE)")))
                .withColumn("TOM", ds2.col("sum(TOM_MALE)").plus(ds2.col("sum(TOM_FEMALE)")));


        // Add all the people of different races in a country
        Dataset ds4 = ds3.withColumn("Total", ds3.col("WA").plus(ds3.col("BA")).
                plus(ds3.col("IA")).plus(ds3.col("AA")).plus(ds3.col("NA"))
                .plus(ds3.col("TOM")));


        // Store the value of  Ni (Total − Ni)/Total^2 in a new column, where Ni is the total number of people
        // in a particular race i.

        Dataset ds5 = ds4.withColumn("DivIndexWA", ds4.col("Total").minus(ds4.col("WA"))
                .multiply(ds4.col("WA")).divide(ds4.col("Total")).divide(ds4.col("Total")))
                .withColumn("DivIndexBA", ds4.col("Total").minus(ds4.col("BA"))
                        .multiply(ds4.col("BA"))
                        .divide(ds4.col("Total")).divide(ds4.col("Total")))
                .withColumn("DivIndexIA", ds4.col("Total").minus(ds4.col("IA"))
                        .multiply(ds4.col("IA"))
                        .divide(ds4.col("Total")).divide(ds4.col("Total")))
                .withColumn("DivIndexAA", ds4.col("Total").minus(ds4.col("AA"))
                        .multiply(ds4.col("AA"))
                        .divide(ds4.col("Total")).divide(ds4.col("Total")))
                .withColumn("DivIndexNA", ds4.col("Total").minus(ds4.col("NA"))
                        .multiply(ds4.col("NA"))
                        .divide(ds4.col("Total")).divide(ds4.col("Total")))
                .withColumn("DivIndexTOM", ds4.col("Total").minus(ds4.col("TOM"))
                        .multiply(ds4.col("TOM"))
                        .divide(ds4.col("Total")).divide(ds4.col("Total")));

        // Calculate the diversity index and then sort by State name and County.
        Dataset ds6 = ds5.withColumn("DiversityIndex",ds5.col("DivIndexWA")
                .plus(ds5.col("DivIndexBA"))
                .plus(ds5.col("DivIndexIA"))
                .plus(ds5.col("DivIndexAA"))
                .plus(ds5.col("DivIndexNA"))
                .plus(ds5.col("DivIndexTOM")))
                .sort(ds.col("STNAME"),ds.col("CTYNAME"));

        // Select just the state name, city name and Diversity index
        Dataset ds7 = ds6.select("STNAME","CTYNAME","DiversityIndex");

        // Create RDD and repartition to 1 file.
        JavaRDD rdd1 = ds7.toJavaRDD().repartition(1);
        // Delete directory if directory present
        deleteDirectory(new File(IDE_Prefix + OutputDirectory));
        // Save the file.
        rdd1.saveAsTextFile(IDE_Prefix + OutputDirectory );

    }

    /**
     *
     * Create a spark conf and using multiple local threads and perform operations needed
     * for calculating the diversity index and saving it to a new file.
     *
     * @param args              No args.
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        // Create a SparkConf that loads defaults from system properties and the classpath
        SparkConf sparkConf = new SparkConf();
        // 8 threads
        sparkConf.set("spark.master", "local[8]");
        //Provides the Spark driver application a name for easy identification in the Spark or Yarn UI
        sparkConf.setAppName("US Consensus");

        // Creating a session to Spark. The session allows the creation of the
        // various data abstractions such as RDDs, DataFrame, and more.
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

        // Creating spark context which allows the communication with worker nodes
        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());

        USDiversityIndex(spark);

        // Stop existing spark context
        jsc.close();

        // Stop existing spark session
        spark.close();
    }
}
