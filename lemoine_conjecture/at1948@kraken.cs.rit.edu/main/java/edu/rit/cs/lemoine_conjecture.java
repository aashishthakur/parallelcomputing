package edu.rit.cs;

/**
 * Class LemoineConjecture provides a class for finding the smallest prime number p,
 * such that any odd number in a given range can be represented as n  = p + 2q
 *
 * @author Aashish Thakur
 * @version 30-Aug-2019
 */

class lemioneConjecture {

    /**
     * Main class to process the conjecture for a given range of odd numbers.
     */
    public static void main(String[] args) {
        int low= 0, high = 0;
        try{
            low = Integer.parseInt(args[0]);
            high = Integer.parseInt(args[1]);
        }catch (NumberFormatException e){
            System.err.println("Out of range");
            System.exit(1);
        }
        int n = 0;
        int range = 0;
        int m = Integer.MIN_VALUE;

        //Check the preconditions regarding the input values.
        if ((low > high) || low <= 5) usage();

        if (!isOdd(low) || !isOdd(high)) {
            usage();
        }
        System.out.println();

        long time = System.currentTimeMillis();

            // omp parallel for
            for (int c = low; c <= high; c += 2) { //1000001 1999999
                int p = returnLC(c);
                if (p > m) {
                    m = p;
                    n = c;
                }
            }


        System.out.println(n+" = "+m + " + 2*" + (n-m)/2 );
        time = System.currentTimeMillis() - time;
        System.out.println(time);
    }


    /***
     * Calculates the p and q value for Lemoine conjecture,
     * formula is, n = p + 2 * q, for odd number n and prime numbers p and q.
     *
     * @param number    The number who's p and q values  are required.
     * @return          returns the value of p.
     */
    private static int returnLC(int number) {
        int result = 0;

        /*
        Every odd number can be represented as n = 2*r + 1
        and we know that, n = p + 2*q, thus we determine
        the max value of q has to be less than r.
        */
        int maxLimitForPrime = (number - 1) / 2 - 1;
        // The p has to be less than value of n - 2 * (max)q
        int pRange = number - maxLimitForPrime;
        //Iterate over prime numbers.
        Prime.Iterator i = new Prime.Iterator();
        i.restart();

        // Check if q value here is prime, if so return the result
        // omp parallel for
        for (int val = i.next(); val < pRange; val = i.next()){
            if (isOdd((number-val)/2)){
                if (Prime.isPrime((number-val)/2)) {
                    return val;
                }
            }

        }

        return result;
    }

    /**
     *
     * Checks if a number is odd or not.
     *
     * @param number            The number to be tested.
     * @return
     */
    private static boolean isOdd(int number) {
        return number % 2 != 0;
    }

    /**
     *
     * Usage for error messages.
     *
     */
    private static void usage() {
        System.err.println("Please choose a valid range such that the odd number is greater than 5" +
                " and upper bound is more than lower bound.");
        System.exit(1);
    }

}
