package edu.rit.cs;
import java.util.Collections;
import java.util.Hashtable;


/**
 * Class Lemoine_Conjecture provides a class for finding the smallest prime number p,
 * such that any odd number in a given range can be represented as n  = p + 2q
 *
 * @author Aashish Thakur
 * @since  30-Aug-2019
 */

public class lemoine_conjecture {

    /**
     * Main class to process the conjecture for a given range of odd numbers.
     * Calls in methods to firstly check if numbers are as per requirement,
     * finds max p in the range of numbers, and prints the same with other
     * required information.
     */
    public static void main(String[] args) {
        int low= 0, high = 0;
        try{
            low = Integer.parseInt(args[0]);
            high = Integer.parseInt(args[1]);
        }catch (NumberFormatException e){
            System.err.println("Please enter two integer numbers.");
            System.exit(1);
        }
        int n;
        int m = Integer.MIN_VALUE;
        Hashtable<Integer, Integer> h = new Hashtable<>();

        //Check the preconditions regarding the input values.
        if ((low > high) || low <= 5) usage();

        if (!isOdd(low) || !isOdd(high)) {
            usage();
        }
            // omp parallel for 
            for (int c = low; c <= high; c += 2) { //1000001 1999999
                int p = returnLC(c);
                // omp critical
                if (p > m) {
                    m = p;
                    n = c;
                    h.put(m,n);
                }
            }

        int max = Collections.max(h.keySet());
        n = h.get(max);
        //Gets the maximum value from the hashset and prints the same.
        System.out.println(n+" = "+max + " + 2*" + (n-max)/2 );
    }


    /***
     * Calculates the p and q value for Lemoine conjecture,
     * formula is, n = p + 2 * q, for odd number n and prime numbers p and q.
     *
     * @param number    The number who's p and q values  are required.
     * @return          returns the value of p.
     */
    private static int returnLC(int number) {
        int result = 0;

        /*
        Every odd number can be represented as n = 2*r + 1
        and we know that, n = p + 2*q, thus we determine
        the max value of q has to be less than r.
        */
        int maxLimitForPrime = (number - 1) / 2 - 1;
        // The p has to be less than value of n - 2 * (max)q
        int pRange = number - maxLimitForPrime;
        //Iterate over prime numbers.
        Prime.Iterator i = new Prime.Iterator();
        i.restart();

        // Check if q value here is prime, if so return the result
        for (int val = i.next(); val < pRange; val = i.next()){
            if (isOdd((number-val)/2)){
                if (Prime.isPrime((number-val)/2)) {
                    return val;
                }
            }

        }

        return result;
    }

    /**
     *
     * Checks if a number is odd or not.
     *
     * @param number            The number to be tested.
     * @return                  True if number is odd else false.
     */
    private static boolean isOdd(int number) {
        return number % 2 != 0;
    }

    /**
     *
     * Usage for error messages.
     *
     */
    private static void usage() {
        System.err.println("Please choose a valid range such that the odd number is greater than 5" +
                " and upper bound is more than lower bound.");
        System.exit(1);
    }

}
