<h2>Breaking RSA</h2>
<p><a name="system"></a></p>
<p>Here is a simplified description of the RSA public key encryption algorithm. The public encryption key consists of two integers (<em>e,&nbsp;n</em>), where&nbsp;<em>e</em>&nbsp;is an exponent and&nbsp;<em>n</em>&nbsp;is a modulus. For this project,&nbsp;<em>e</em>&nbsp;=&nbsp;3 and&nbsp;<em>n</em>&nbsp;is either a prime or the product of two primes. To encrypt a plaintext message&nbsp;<em>m</em>, where&nbsp;<em>m</em>&nbsp;is an integer in the range 0 through&nbsp;<em>n</em>&minus;1, compute the following formula yielding the ciphertext&nbsp;<em>c</em>, where&nbsp;<em>c</em>&nbsp;is also an integer in the range 0 through&nbsp;<em>n</em>&minus;1:</p>
<p></p>
<center><em>c</em>&nbsp;=&nbsp;<em>m</em><sup>3</sup>&nbsp;(mod&nbsp;<em>n</em>)</center>
<p>To decrypt the ciphertext, normally you have to know the private decryption key that corresponds to the public encryption key. However, you can also decrypt the ciphertext without knowing the private key by computing a modular cube root:</p>
<p></p>
<center><em>m</em>&nbsp;=&nbsp;<em>c</em><sup>1/3</sup>&nbsp;(mod&nbsp;<em>n</em>)</center>
<p>If&nbsp;<em>n</em>&nbsp;is a large integer, e.g. a 2048-bit integer, no one knows an efficient algorithm to compute a modular cube root. But if&nbsp;<em>n</em>&nbsp;is small, it becomes possible to compute the modular cube root by brute force search: Given a ciphertext&nbsp;<em>c</em>, compute&nbsp;<em>m</em><sup>3</sup>&nbsp;(mod&nbsp;<em>n</em>) for every&nbsp;<em>m</em>, 0&nbsp;&le;&nbsp;<em>m</em>&nbsp;&le;&nbsp;<em>n</em>&minus;1, until the answer equals&nbsp;<em>c</em>.</p>
<p>Depending on the particular values of&nbsp;<em>c</em>&nbsp;and&nbsp;<em>n</em>,&nbsp;<em>c</em>&nbsp;might have anywhere from zero to three modular cube roots&mdash;that is, zero to three values of&nbsp;<em>m</em>&nbsp;such that&nbsp;<em>c</em>&nbsp;=&nbsp;<em>m</em><sup>3</sup>&nbsp;(mod&nbsp;<em>n</em>).</p>


<p>
Results:

at1948@kraken:~/pctest/parallelcomputing/cuda_examples/src/main/cpp$ ./bin/VectorAdditionGPUver1 46054145 124822069
142857^3 = 46054145 (mod 124822069)
27549958^3 = 46054145 (mod 124822069)
97129254^3 = 46054145 (mod 124822069)</p>


<p>With nvprof:</p>

<p>==9328== Profiling application: ./bin/VectorAdditionGPUver1 46054145 124822069
<p>==9328== Profiling result:</p>
<p>            Type  Time(%)      Time     Calls       Avg       Min       Max  Name</p>
 <p>GPU activities:   99.99%  34.067ms         1  34.067ms  34.067ms  34.067ms  add(__int64, __int64, __int64*)</p>
 <p>                   0.01%  1.8560us         1  1.8560us  1.8560us  1.8560us  [CUDA memcpy DtoH]</p>
 <p>                   0.00%  1.5040us         1  1.5040us  1.5040us  1.5040us  [CUDA memcpy HtoD]</p>
 <p>     API calls:   89.45%  531.79ms         1  531.79ms  531.79ms  531.79ms  cudaMalloc</p>
   <p>                  5.83%  34.638ms         2  17.319ms  21.966us  34.616ms  cudaMemcpy</p>
   <p>                  4.28%  25.425ms       384  66.212us     284ns  23.309ms  cuDeviceGetAttribute</p>
   <p>                  0.35%  2.0579ms         4  514.47us  510.56us  520.79us  cuDeviceTotalMem</p>
    <p>                 0.04%  221.10us         1  221.10us  221.10us  221.10us  cudaFree</p>
    <p>                 0.03%  195.88us         1  195.88us  195.88us  195.88us  cudaLaunchKernel</p>
     <p>                0.03%  187.43us         4  46.857us  44.904us  51.795us  cuDeviceGetName</p>
       <p>              0.00%  8.4860us         4  2.1210us  1.3110us  3.3700us  cuDeviceGetPCIBusId</p>
      <p>               0.00%  4.2030us         8     525ns     314ns     949ns  cuDeviceGet</p>
       <p>              0.00%  1.9200us         3     640ns     242ns     910ns  cuDeviceGetCount</p>
        <p>             0.00%  1.4820us         4     370ns     317ns     443ns  cuDeviceGetUuid</p>

 <p>==9328== Unified Memory profiling result:</p>
 <p>Device "Tesla K40c (0)"</p>
 <p>   Count  Avg Size  Min Size  Max Size  Total Size  Total Time  Name</p>
  <p>      2  32.000KB  4.0000KB  60.000KB  64.00000KB  11.36000us  Device To Host</p>
 <p>Total CPU Page faults: 1</p>