#include <iostream>
#include <vector>

/*
Perform the m^3 mod n operation and check if the values match with the cipher text
in case a match is found, append it to an array. Using a single thread of a
GPU, in this kernel.

@param n        This is n with which we will take mod.
@param c        Ciphertext used for comparison.
@param v        Array where we will store the results.

*/
__global__
void add(unsigned long long int n, unsigned long long int c,
        unsigned long long int *v)
{

    unsigned long long int cPred;
    int i = 0;
    for (unsigned long long int m = 0;  m<n ; m++) {
        cPred = (((m * m)%n)*m)%n;
        if(cPred==c){
            v[i] = m;
            i++;
        }
    }
}

/*
Take input from the user and check if c and n are passed. If so,
proceed to find values, w.r.t values found print them or show
that no values were found.

@param argc     How many arguments were passed.
@param argv[]   The values of c and n.
@return         default return 0, return 1 if any issue.

*/
int main(int argc, char* argv[])
{
    unsigned long long int c;
    unsigned long long int n;
    if (argc==3){
        c = std::stoull(argv[1]);
        n = std::stoull(argv[2]);
    } else{
        printf("Please pass values of c and n");
        exit(1);
    }

    auto *v = new unsigned long long int[100];
    // Allocate vectors in device memory
    unsigned long long int* d_v;
    cudaMalloc(&d_v, 100);

    // Copy vectors from host memory to device memory
    cudaMemcpy(d_v, v, 100, cudaMemcpyHostToDevice);

    // Invoke kernel
    add<<<1, 1>>>(n,c,d_v);


    // Copy result from device memory to host memory
    // h_C contains the result in host memory
    cudaMemcpy(v, d_v, 100, cudaMemcpyDeviceToHost);

    for (int j = 0; j < 100; ++j) {
        if(v[j]==0){
            break;
        }
        std::cout << "Val " << v[j] << std::endl;
    }

    if(v[0]==0){
        std::cout << "No cube roots of "<< c << " (mod "
                                        << n << ")"<< std::endl;
        }
    // Free device memory
    cudaFree(d_v);
    delete [] v;
    return 0;
}