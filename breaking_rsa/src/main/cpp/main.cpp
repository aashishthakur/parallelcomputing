#include <iostream>
#include <vector>

/*
Perform the m^3 mod n operation and check if the values match with the cipher text
in case a match is found, append it to an array.

@param n        This is n with which we will take mod.
@param c        Ciphertext used for comparison.
@param v        Array where we will store the results.

@return        Shows how many values are present.

*/

int add(unsigned long long int n, unsigned long long int c,
        unsigned long long int *v)
{
    unsigned long long int m;
    unsigned long long int cPred;
    int i = 0;
    //Loop over all the values
    for (m = 0;  m<n ; m++) {
        cPred = (((m * m)%n)*m)%n;
        if(cPred==c){
            v[i] = m;
            i++;
        }
    }
    return i;
}

/*
Take input from the user and check if c and n are passed. If so,
proceed to find values, w.r.t values found print them or show
that no values were found.

@param argc     How many arguments were passed.
@param argv[]   The values of c and n.
@return         default return 0, return 1 if any issue.

*/
int main(int argc, char* argv[])
{
    unsigned long long int c;
    int i = 0;
    unsigned long long int n;
    auto *v = new unsigned long long int[100];
    if (argc==3){
        c = std::stoull(argv[1]);
        n = std::stoull(argv[2]);
    } else{
        printf("Please pass values of c and n");
        exit(1);
    }
    i = add(n,c,v);

    for (int j = 0; j < i; ++j) {
        if (v[j] == 0) {
            break;
        }
        std::cout << v[j] <<"^3 = " << c << " (mod "
                  << n << ")"<< std::endl;
    }
    if(i==0){
        std::cout << "No cube roots of "<< c << " (mod "
                  << n << ")"<< std::endl;
    }
    delete [] v;
    return 0;
}
