#include <iostream>
#include <vector>

// Variable shared across both the host and the device.
__managed__ int i=0;

/*
This is the kernel of GPU, here we need to get the thread posiiton using the formula
shown in code and get a stride value to get appropriate chunk size, using which we
calculate the
Perform the m^3 mod n operation and check if the values match with the cipher text
in case a match is found, append it to an array. Using a single thread of a
GPU, in this kernel.

@param n        This is n with which we will take mod.
@param c        Ciphertext used for comparison.
@param v        Array where we will store the results.

*/
__global__
void add(unsigned long long int n, unsigned long long int c,
        unsigned long long int *v)
{
    unsigned long long int cPred;
    // Get the thread position, this is the value of m with which
    // we will take the mod of n using the stride.
    unsigned long long int m = blockDim.x * blockIdx.x + threadIdx.x;
    // Stride will be the chunk or block size.
    int stride = blockDim.x * gridDim.x;
    for (unsigned long long int k = m; k < n; k += stride){
        cPred = ((((k % n)* k)% n)* k)% n;
        if(cPred==c){
            v[atomicAdd(&i, 1)] = k;
        }
    }
}

/*
Take input from the user and check if c and n are passed. If so,
proceed to find values, w.r.t values found print them or show
that no values were found.

@param argc     How many arguments were passed.
@param argv[]   The values of c and n.
@return         default return 0, return 1 if any issue.

*/
int main(int argc, char* argv[])
{
    unsigned long long int c;
    unsigned long long int n;
    if (argc==3){
        c = std::stoull(argv[1]);
        n = std::stoull(argv[2]);
    } else{
        std::cerr << "Please pass values of c and n" << std::endl;
        exit(1);
    }
    auto v = (unsigned long long int*)malloc(100);
    // Allocate vectors in device memory
    unsigned long long int* d_v;
    cudaMalloc(&d_v, 100);

    // Copy vectors from host memory to device memory
    cudaMemcpy(d_v, v, 100, cudaMemcpyHostToDevice);

    // Invoke kernel
    int threadsPerBlock = 256;
    int blocksPerGrid = (n + threadsPerBlock - 1) / threadsPerBlock;
    add<<<blocksPerGrid, threadsPerBlock>>>(n,c,d_v);

    // Copy result from device memory to host memory
    // h_C contains the result in host memory
    cudaMemcpy(v, d_v, 100, cudaMemcpyDeviceToHost);


    //Sort the results obtained.
    for (int k = 0; k < i-1; ++k) {
        for (int j = 0; j < i-k-1; ++j) {
            if(v[j]>v[j+1]){
                unsigned long long int temp = v[j];
                v[j] = v[j+1];
                v[j+1] = temp;
            }
        }
    }

    //Print the results.
    for (int j = 0; j < i; ++j) {
        std::cout << v[j] <<"^3 = " << c << " (mod "
                << n << ")"<< std::endl;
    }

    //If no values found
    if(i==0){
        std::cout << "No cube roots of "<< c << " (mod "
                                        << n << ")"<< std::endl;
        }

    // Free device memory
    cudaFree(d_v);
    delete [] v;
    return 0;
}